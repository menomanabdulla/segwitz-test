# Hello, Many greetings Team Segwitz.

For better experience as an inspector, Here is some points you could concerened:
1. This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
2. This project build with [json-server](https://www.npmjs.com/package/json-server) for application dummy data.
3. This project build with CSS in JS concenpt with [styled-components](https://styled-components.com/)
4. Most Importantly I implemented UML diagram of this project in project root folder. 

## Available Scripts

In the project directory, you can run:

### `yarn json-server`

Runs the json-server at [http://localhost:3002](http://localhost:3002) to using this localhost-ports in further uses of this application .


### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3009](http://localhost:3009) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

To-be-honest I was in time-catastrophe, maybe I could add more-value. Despite everything  I tried to meet all the requirments mentiond in task list. If there any issue makes any confusion please do let me know. I would love any words, Thanks. 
